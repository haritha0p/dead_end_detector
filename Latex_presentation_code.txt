\documentclass{beamer}
\usepackage[utf8]{inputenc}
\usefonttheme[onlymath]{serif}
\definecolor{LHCblue}{RGB}{4, 114, 255}
\usecolortheme[named=LHCblue]{structure}
\usepackage[bars]{beamerthemetree} % Beamer theme v 2.2
\usepackage{multicol}
\usepackage{lmodern}
\usepackage{lipsum}
\usepackage{graphicx}
\usepackage{marvosym}
\usepackage[english]{babel}
\usepackage{ragged2e}
\usepackage{blindtext}

\begin{figure}
\includegraphics[width=5cm]{R.png}
\end{figure}

\title{\textbf{Dead End Detector}}
\author{
21B01A0321 & G.Geetika & (MECH)\newline
21B01A6161 & Y.Poojitha & (AIML)\newline
21B01A0477 & P.Haritha & (ECE-B)\newline
21B01A5431 & D.Nandini & (AIDS-A)\newline
21B01A5425 & D.Surekha & (AIDS-A)
}
\date{\RaggedLeft February 4, 2023}

\begin{document}

\maketitle
%creating a frame
\begin{frame}{Introduction}
\textbf{Problem statement}
\begin{itemize}
    \item To improve road sign placement in a road map especially for dead ends in order to find dead ends easily.
\end{itemize}
\begin{figure}
\includegraphics[width=8cm]{ded.jpeg}
\end{figure}
\end{frame}

%Approach

\begin{frame}{Approach}
    \begin{itemize}
        \item We solved this problem by recursion approach. 
        \item All the nodes are traversed in list named "list1" the function "detect\textunderscore dead\textunderscore ends" looks for edges and then it checks whether the edge is dead end or not.
        \item The function "filter\textunderscore unwanted" separate all the nodes into two categories (wanted, unwanted).
        \item All the filtered nodes are deleted in
        "remove\textunderscore unwanted" function.
        \item At last all the functions are called in main function "input\textunderscore command\textunderscore line()"
    \end{itemize}
\end{frame}

%Learnings

\begin{frame}{Learnings}
\begin{itemize}
    \item Modules (sys module)
\end{itemize}
\end{frame}

%Challenges

\begin{frame}{Challenges}
\begin{itemize}
    \item We faced the problems at giving input through command line arguments.
    \item By referring some websites, watching videos we confronted the issue we faced.
\end{itemize}
\end{frame}

%Statistics

\begin{frame}{Statistics}
\begin{itemize}
    \item Number of lines of code - 95
    \item Number of functions - 5\newline
    detect\textunderscore dead\textunderscore ends()\newline        
    traverse()\newline
    filter\textunderscore unwanted()\newline                        
    remove\textunderscore unwanted()\newline
    input\textunderscore command\textunderscore line()
\end{itemize}
\end{frame}

%DEMO/ Screenshots

\begin{frame}{Demo/ Screenshots}
\begin{figure}
\includegraphics[width=10cm]{output1.jpeg}
\caption{\label{fig:Demo}Output-1}
\end{figure}
\end{frame}

\begin{frame}{Demo/ Screenshots}
\begin{figure}
\includegraphics[width=10cm]{output2.jpeg}
\caption{\label{fig:Demo}Output-2}
\end{figure}
\end{frame}


%THANKYOU

\begin{frame}{END}
\begin{Huge}
\begin{center}
\textbf{\textit & THANK YOU}
\end{center}
\end{Huge}
\end{frame}

\section{Introduction}
\end{document} 